// Copyright Epic Games, Inc. All Rights Reserved.

#include "UE4Workshop.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UE4Workshop, "UE4Workshop" );
 