// Copyright Epic Games, Inc. All Rights Reserved.

#include "UE4WorkshopGameMode.h"
#include "UE4WorkshopCharacter.h"
#include "UObject/ConstructorHelpers.h"

AUE4WorkshopGameMode::AUE4WorkshopGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Soluce/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
