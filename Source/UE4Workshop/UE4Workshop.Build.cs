// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class UE4Workshop : ModuleRules
{
	public UE4Workshop(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
